<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
<div align="center">
<header>
		<img src="spup-logo.png" style="float:left;width:120px;height:100px;">
   		<h1 style="font-family: Old English Text MT; color: green">St. Paul University Philippines</h1>
   		<h2 style="font-family: Times New Roman; color: green"> Tuguegarao City, Cagayan 3500</h2>
</header>
Three Paulinian student-leaders joined the recently concluded 2017 Global Cultural Exchange Summer Camp at Chang Jung Christian University (CJCU), Tainan City, Taiwan, last August 21-26, 2017. SPUP was represented by the officers of the Paulinian Student Government (PSG) namely: Linus Janvier Pagulayan, (President), Fatima Sheryl Agcaoili (Executive Secretary), and Allaine Azada (Chief Justice).<br> <br>

The summer camp was organized by the Language Education Center and the Office of International Affairs at CJCU. It was designed to foster understanding regarding cultural diversity, sport a global vision and foster closer ties among the participants from different Asian countries.<br> <br>

The activity featured CJCU-Spotlight Social Business where groups drafted and thought of businesses which considers social mission as its core to success. It was facilitated by newly founded Taiwanese branch of Yunus Social Business Center at CJCU. Moreover, the participants had also engaged in Taiwan art and history, do-it-yourself Taiwan cuisine, innovation and creativity workshops, cultural boot camp presentations, and trips around Taiwan.<br> <br>

The summer camp was graced by 78 students who had undergone a prerequisite application process— 33 freshmen students from CJCU, and 45 foreign students coming from Philippines, Japan, Indonesia, Malaysia, Thailand, South Korea, India.<br> <br>
</body>
</html>