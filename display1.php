<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
<div align="center">
<header>
		<img src="spup-logo.png" style="float:left;width:120px;height:100px;">
   		<h1 style="font-family: Old English Text MT; color: green">St. Paul University Philippines</h1>
   		<h2 style="font-family: Times New Roman; color: green"> Tuguegarao City, Cagayan 3500</h2>
</header>
SPUP's Health Services Unit (HSU) initiated an Outreach Activity at the Regional Haven for Women and Children (RHWC) in Maddarulug, Solana on September 2, 2017. Dr. Maricon Manuel (Head, Health Services Unit) catered to the Medical needs of the residents of the center; while, Dr. Grace Tamayao and Dr. Angelica Asuncion (University Dentists) conducted Dental services to the children at the center. Ms. Badet Quiambao, Mr. Jaypee Talosig and Ms. Zylla Ezra Tenedero (University Nurses) manned the feeding program activity. Former University Nurses, Ms. Danica Lim (now working at St. Paul Hospital) and Ms. Preiyanne Kaye Ulep (now based in New Jersey, USA) extended their help by rendering their services to the Outreach Activity. The HSU Personnel also gave simple gifts to the children as advanced-presents for the Yuletide season.<br> <br>

***SPUP-MEDTECH YIELDS 90.91% PASSING RATE<br> <br>

2017-08-31<br> <br>

Caritas Christi urget nos!<br> <br>

SPUP's College of Medical Technology (MedTech) registered a passing rate of 90.91%. This was announced by the Professional Regulation Commission (PRC) as it released the result of the Medical Technology Licensure Examinations on August 31,2017. The Commission also published that this year's MedTech Exams yielded a national passing rate of 85.16%. This meant that 4,821 out of 5,661 examinees passed the board exam which was conducted last August 26-27,2017.<br> <br>
</body>
</html>