<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
<div align="center">
<header>
		<img src="spup-logo.png" style="float:left;width:120px;height:100px;">
   		<h1 style="font-family: Old English Text MT; color: green">St. Paul University Philippines</h1>
   		<h2 style="font-family: Times New Roman; color: green"> Tuguegarao City, Cagayan 3500</h2>
</header>
August 30, 2017 - St. Paul University Philippines (SPUP) through the Intercultural Institute for Languages (IIL) and the Graduate School, awarded Certificate of Completion and Certificate of Recognition to Taiwanese Language Interns from the Chang Jung Christian University (CJCU) namely Hu Shih-Yu, Wu Chih-Ying, and Huang Yu-Ying.
<br> <br>

These interns have completed their one-month internship program at SPUP doing translation activities by teaching Chinese as a Foreign Language to doctoral students in various fields. Sister Merceditas Ang, SPC (University President), headed the ceremony and witnessed by academic leaders. This exchange activity is for continuing implementation of the bilateral agreement of CJCU and SPUP for four years now.<br> <br>
</body>
</html>