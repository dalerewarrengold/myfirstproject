<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
<div align="center">
<header>
		<img src="spup-logo.png" style="float:left;width:120px;height:100px;">
   		<h1 style="font-family: Old English Text MT; color: green">St. Paul University Philippines</h1>
   		<h2 style="font-family: Times New Roman; color: green"> Tuguegarao City, Cagayan 3500</h2>
</header>
</div>
The Paulinian Volunteers for Community Development (PVCD) Club was honoured as OUTSTANDING UNESCO CLUB (Education Category) and obtained a Certificate of GOOD STANDING during the annual assembly of UNESCO Clubs in the Philippines. Moreover, Mrs. Noemi Cabaddu (CES Director and PVCD Adviser) was awarded OUTSTANDING UNESCO CLUB EDUCATOR; while, Ms. Cheeni Mabbayad (PVCD President) was hailed as OUTSTANDING UNESCO CLUB YOUTH LEADER. The awards were presented during the 2017 International Assembly of Youth for UNESCO held at the Icon Hotel, Quezon City on September 01-03, 2017.<br> <br>
The awarding served as one of the highlights of this year’s conference organized by the National Association of UNESCO Clubs in the Philippines (NAUCP), which was participated in by more than 300 participants from the country and abroad. Mr. Jonathan Guerero (President, NAUCP) and Dr. Serafin Arviola, Jr. (Chairman, NAUCP) led the recognition of the awardees.<br> <br>
(SPUP's PVCD is the FIRST UNESCO-Recognized Student CLUB in REGION II and the ONLY in the St. Paul University System. It is also the ONLY RECIPIENT from the Cagayan Valley Region of this year's Outstanding award.)<br> <br>
</body>
</html>