<!DOCTYPE html>
<html lang="en">
<body style="background-color:#e0e2e5">
<center>
<head>
	<title>St. Paul University Philippines</title>
<style>
div.container {
    width: 100%;
}

header, footer {
    padding: 1em;
    color: white;
    background-color: white;
    clear: left;
    text-align: center;
}
</style>
</head>
<body>
<div class="container">

<header>
		<img src="spup-logo.png" style="float:left;width:120px;height:100px;">
   		<h1 style="font-family: Old English Text MT; color: green">St. Paul University Philippines</h1>
   		<h2 style="font-family: Times New Roman; color: green"> Tuguegarao City, Cagayan 3500</h2>
</header>

<body>

	<div class="body">
		<h2 style="color:green;" align="center"><u>NEWS</u></h2>
		<br>
		<img src="images/pic1.jpg" width="440" height="320">
		<p align="center">SPUP’S PVCD REAPS AWARDS IN UNESCO CLUBS INTERNATIONAL ASSEMBLY</p>
		<p align="center">Caritas Christi urget nos! </p>
		<p>(by: Ms. Noemi Cabaddu and Dr. Allan Peejay Lappay)</p>
		<a href="display.php"><button type="button">Read more</button></a>
		<br> <br>

		<img src="images/pic2.jpg" width="440" height="320">
		<p align="center">SPUP'S HEALTH SERVICES REACHES OUT TO RHWC</p>
		<p align="center">Caritas Christi urget nos! </p>
		<p>((by: Ms. Zylla Ezra Tenedero and Mr. Jaypee Talosig)</p>
		<a href="display1.php"><button type="button">Read more</button>
		<br> <br> </a>

		<img src="images/pic3.jpg" width="800" height="320">
		<p align="center">SPUP-MEDTECH YIELDS 90.91% PASSING RATE</p>
		<p align="center">Caritas Christi urget nos! </p>
		<a href="display2.php"><button type="button">Read more</button>
		<br> <br> </a>

		<img src="images/pic4.jpg" width="500" height="320">
		<p align="center">SPUP RECOGNIZES INTERNS FROM CJCU, TAIWAN</p>
		<p align="center">Caritas Christi urget nos! </p>
		<p>(by: Dr. Jeremy Godofredo Morales)</p>
		<a href="display3.php"><button type="button">Read more</button>
		<br> <br> </a>

		<img src="images/pic5.jpg" width="480" height="320">
		<p align="center">SPUP INVITES PNP-TUGUEGARAO FOR SHS SAFETY AND SECURITY ORIENTATION</p>
		<p align="center">Caritas Christi urget nos! </p>
		<a href="display4.php"><button type="button">Read more</button>
		<br> <br> </a>
	</div>
</center>
</body>
</html>