<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
<div align="center">
<header>
		<img src="spup-logo.png" style="float:left;width:120px;height:100px;">
   		<h1 style="font-family: Old English Text MT; color: green">St. Paul University Philippines</h1>
   		<h2 style="font-family: Times New Roman; color: green"> Tuguegarao City, Cagayan 3500</h2>
</header>
 As part of the week-long Orientation Program for the Senior High School (SHS), SPUP has invited officers from the Philippine National Police (PNP) – Tuguegarao City Station on September 07, 2017, to lead the discussion on Students’ Safety and Security. This is in accordance with the advocacy of SPUP, which aims to empower students towards peace-building engagement activities.<br> <br>

PSI Alexander Tamang, lectured on Crime and Drug Prevention; while, PSI Ronilyn Baccay discussed the Anti-Violence Against Women and Children Act (RA 9262). Their presentation was followed by an open forum in which the students interacted by posing questions to the speakers. SHS students intently listened as the experts talked about practical approaches and responses to prevent the occurrence of crimes, abuses and violence. Moderated by Dr. Allan Peejay Lappay (Director, Alumni, External Relations and Advocacies), the interaction fostered an avenue for a cheerful yet serious discussion on how to promote safety and security especially when students are outside SPUP. PSI Tamang and PSI Baccay provided safety and security tips as they responded to the many queries of the participants. <br> <br>

The session ended with the recognition of the speakers and the officers of PNP-Tuguegarao City, led by Dr. Pilar Acorda (Dean, School of Arts, Sciences and Teacher Education) and Ms. Alma Quinagoran (Director, Office of Student Affairs). 
<br> <br>